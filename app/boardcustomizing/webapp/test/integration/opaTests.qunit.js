sap.ui.require(
    [
        'sap/fe/test/JourneyRunner',
        'boardcustomizing/test/integration/FirstJourney',
		'boardcustomizing/test/integration/pages/BoardGradesList',
		'boardcustomizing/test/integration/pages/BoardGradesObjectPage',
		'boardcustomizing/test/integration/pages/PaperMaterialsObjectPage'
    ],
    function(JourneyRunner, opaJourney, BoardGradesList, BoardGradesObjectPage, PaperMaterialsObjectPage) {
        'use strict';
        var JourneyRunner = new JourneyRunner({
            // start index.html in web folder
            launchUrl: sap.ui.require.toUrl('boardcustomizing') + '/index.html'
        });

       
        JourneyRunner.run(
            {
                pages: { 
					onTheBoardGradesList: BoardGradesList,
					onTheBoardGradesObjectPage: BoardGradesObjectPage,
					onThePaperMaterialsObjectPage: PaperMaterialsObjectPage
                }
            },
            opaJourney.run
        );
    }
);