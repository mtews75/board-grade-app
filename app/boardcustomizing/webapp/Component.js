sap.ui.define(
    ["sap/fe/core/AppComponent"],
    function (Component) {
        "use strict";

        return Component.extend("boardcustomizing.Component", {
            metadata: {
                manifest: "json"
            }
        });
    }
);