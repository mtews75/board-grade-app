using BoardGradeService as service from '../../srv/boardgrade-service';

annotate service.BoardGrades with @(
    UI.LineItem : [
        {
            $Type : 'UI.DataField',
            Label : 'Board ID',
            Value : boardID,
        },
        {
            $Type : 'UI.DataField',
            Label : 'Flute',
            Value : flute,
        },
        {
            $Type : 'UI.DataField',
            Label : 'Valid From',
            Value : validFrom,
        },
        {
            $Type : 'UI.DataField',
            Label : 'Valid To',
            Value : validTo,
        },
        {
            $Type : 'UI.DataField',
            Label : 'Starch',
            Value : starchCode_code,
        },
    ]
);
annotate service.BoardGrades with @(
    UI.FieldGroup #GeneratedGroup1 : {
        $Type : 'UI.FieldGroupType',
        Data : [
            {
                $Type : 'UI.DataField',
                Label : 'Board ID',
                Value : boardID,
            },
            {
                $Type : 'UI.DataField',
                Label : 'Flute',
                Value : flute,
            },
            {
                $Type : 'UI.DataField',
                Label : 'Valid From',
                Value : validFrom,
            },
            {
                $Type : 'UI.DataField',
                Label : 'Valid To',
                Value : validTo,
            },
            {
                $Type : 'UI.DataField',
                Label : 'Starch Type',
                Value : starchCode_code,
            },
            {
                $Type : 'UI.DataField',
                Label : 'Description',
                Value : description,
            },
        ],
    },
    UI.Facets : [
        {
            $Type : 'UI.ReferenceFacet',
            ID : 'GeneratedFacet1',
            Label : 'General Information Board',
            Target : '@UI.FieldGroup#GeneratedGroup1',
        },
        {
            $Type : 'UI.ReferenceFacet',
            Label : 'Paper Materials',
            Target : 'papermaterials/@UI.LineItem',
        },
        {
            $Type : 'UI.ReferenceFacet',
            Label : 'Plants',
            Target : 'plants/@UI.LineItem',
        },
    ]
);
annotate service.PaperMaterials with @(UI : {
    HeaderInfo  : {
        $Type : 'UI.HeaderInfoType',
        TypeName : 'Paper Material',
        TypeNamePlural : 'Paper Materials',
        Description    : {
            $Type : 'UI.DataField',
            Value : paperMat
        }
    },

    SelectionFields     : [],

    LineItem            : [
        {
            $Type   : 'UI.DataField',
            Label   : 'Paper ID',
            Value   : paperID_code,
        },
        {
            $Type   : 'UI.DataField',
            Label   : 'Paper Material',
            Value   : paperMat,
        },
        {
            $Type   : 'UI.DataField',
            Label   : 'Paper Description',
            Value   : papMatDescr,
        }
        
    ],

});

annotate service.Plants with @(UI : {
    HeaderInfo  : {
        $Type : 'UI.HeaderInfoType',
        TypeName : 'Plant',
        TypeNamePlural : 'Plants',
        Description    : {
            $Type : 'UI.DataField',
            Value : plant
        }
    },

    SelectionFields     : [],

    LineItem            : [
        {
            $Type   : 'UI.DataField',
            Label   : 'Plant',
            Value   : plant,
        },
        {
            $Type   : 'UI.DataField',
            Label   : 'Description',
            Value   : description,
        }    
        
    ],

})

{};
