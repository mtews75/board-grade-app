## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Thu Aug 10 2023 15:46:48 GMT+0200 (Mitteleuropäische Sommerzeit)|
|**App Generator**<br>@sap/generator-fiori-elements|
|**App Generator Version**<br>1.10.1|
|**Generation Platform**<br>Visual Studio Code|
|**Template Used**<br>List Report Page V4|
|**Service Type**<br>Local Cap|
|**Service URL**<br>http://localhost:4004/board-grade/
|**Module Name**<br>boardcustomizing|
|**Application Title**<br>Maintain Board Grades|
|**Namespace**<br>|
|**UI5 Theme**<br>sap_horizon|
|**UI5 Version**<br>1.117.0|
|**Enable Code Assist Libraries**<br>False|
|**Enable TypeScript**<br>False|
|**Add Eslint configuration**<br>False|
|**Main Entity**<br>BoardGrades|
|**Navigation Entity**<br>papermaterials|

## boardcustomizing

A Fiori application.

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply start your CAP project and navigate to the following location in your browser:

http://localhost:4004/boardcustomizing/webapp/index.html

#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


