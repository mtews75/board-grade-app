using {managed,cuid}        from '@sap/cds/common';
using {sap.common.CodeList} from '@sap/cds/common';


// Starch Types
type StarchTypeCode  : String(3);

entity StarchTypes : CodeList {
    key code : StarchTypeCode @(title: 'Starch Type');
}

annotate StarchTypes with {
    code @Common.Text: name;
}

type StarchType      : Association to StarchTypes;
// Paper ID Types
type PaperIdTypeCode : String(2);

entity PaperIdTypes : CodeList {
    key code : PaperIdTypeCode @(title: 'Paper ID')
}

annotate PaperIdTypes with {
    code @Common.Text: name;
}

type PaperIdType     : Association to PaperIdTypes;


entity BoardGrades : managed, cuid {
    boardID        : String(15)                @mandatory;
    flute          : String(3)                 @mandatory;
    validFrom      : Date default $now         @mandatory;
    validTo        : Date default '9999-12-31' @mandatory;
    starchCode     : StarchType;
    description    : String(70);
    papermaterials : Composition of many PaperMaterials
                         on papermaterials.boardID = $self;
    plants         : Composition of many Plants 
                        on plants.boardID = $self;
}

entity PaperMaterials : managed, cuid {
    boardID     : Association to BoardGrades;
    paperID     : PaperIdType;
    paperMat    : String(80);
    papMatDescr : String(70);
}

entity Plants : managed, cuid {
    boardID     : Association to BoardGrades;
    plant       : String(4) @mandatory;
    description : String(70);
}