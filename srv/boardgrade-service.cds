using { BoardGrades as BoardGradesDB } from '../db/schema';

service BoardGradeService {
    entity BoardGrades as projection on BoardGradesDB;
    annotate BoardGrades with @data.draft.enabled;
}